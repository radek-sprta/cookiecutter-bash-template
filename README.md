# Cookiecutter: Bash Template
Cookiecutter template for Bash scripts. Saves time by bootstrapping your repository. Includes a handy bash template.

## Repository features
- Choose license
- Prepare documentation
- Configure shfmt pre-commit hook
- Setup Gitlab CI for documentation and linting

## Template features
- Colored output
- Depency checks
- Error handling
- Root privilege check

## Usage
To use the template, you have to install Cookiecutter. Simply use pip for the task:

`pip install cookiecutter`

And then run it to bootstrap your project:

`cookiecutter gl:radek-sprta/cookiecutter-bash-template`

For more info, refer to the [documentation][documentation].

## Contributing
For information on how to contribute to the project, please check the [Contributor's Guide][contributing].

## Contact
[mail@radeksprta.eu](mailto:mail@radeksprta.eu)

[incoming+radek-sprta/cookiecutter-bash-template@gitlab.com](incoming+radek-sprta/cookiecutter-bash-template@gitlab.com)

## License
MIT

## Credits

This package was created with [Cookiecutter][cookiecutter].

[contributing]: https://gitlab.com/radek-sprta/cookiecutter-bash-template/blob/master/CONTRIBUTING.md
[cookiecutter]: https://github.com/audreyr/cookiecutter
[documentation]: https://radek-sprta.gitlab.io/cookiecutter-bash-template
