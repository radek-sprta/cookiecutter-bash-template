# {{ cookiecutter.project_name }}

{{ cookiecutter.description }}

## Features
- TODO

## Installation
Just clone the repository:
`git clone https://{{ cookiecutter.git_server }}/{{ cookiecutter.git_name }}/{{ cookiecutter.project_slug }}`

## Usage
- TODO

## Contributing
For information on how to contribute to the project, please check the [Contributor's Guide][contributing].

## Contact
[{{ cookiecutter.email }}](mailto:{{ cookiecutter.email }})

{%- if cookiecutter.git_server in ('gitlab.com', 'github.com') -%}
[incoming+{{ cookiecutter.git_name }}/{{ cookiecutter.project_slug }}@{{ cookiecutter.git_server }}](incoming+{{ cookiecutter.git_name }}/{{ cookiecutter.project_slug }}@{{ cookiecutter.git_server }})
{%- endif %}

## License
{{ cookiecutter.license }}

## Credits
This package was created with [Cookiecutter][cookiecutter].

[contributing]: https://{{ cookiecutter.git_server }}/{{ cookiecutter.git_name }}/{{ cookiecutter.project_slug }}/blob/master/CONTRIBUTING.md
[cookiecutter]: https://github.com/audreyr/cookiecutter
