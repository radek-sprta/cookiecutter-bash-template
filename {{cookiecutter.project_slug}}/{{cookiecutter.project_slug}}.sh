#!/usr/bin/env bash
###
### {{ cookiecutter.description }}
###
### Usage:
###  {{ cookiecutter.project_slug }}.sh
###
### -h --help   Show this screen.
###
### Author: {{ cookiecutter.author }} <{{ cookiecutter.email }}>

set -o errexit  # Error if a command fails
set -o errtrace # Inherit error trap
set -o nounset  # Error if a variable is unset
set -o pipefail # Error if pipe fails
#set -o xtrace  # Debug mode
IFS=$'\n\t' # Make iterations more predictible

# Main control flow
function main() {
    trap script_error ERR
    trap "echo cleanup" EXIT
    parse_params "$@"
    echo "${PARAMETER}"
}

# Check if function has the right amount of arguments.
# Args: $1 (required): Actual number of arguments. Always pass "$#".
#       $2 (required): Number of arguments function accepts. Always pass "$#".
#       $3 (optional): Upper range of number of arguments.
function args_number() {
    if ! [[ "$1" -ge "$2" ]] && [[ "$1" -le "${3:-$2}" ]]; then
        script_error "Function requires $2 arguments!"
    fi
}

# Print string in given color. Supports green, red and yellow.
# Args: $1 (required): Color to use.
#       $2 (required): String to print.
function cprint() {
    args_number $# 2
    declare -A colors
    local colors=(
        ["green"]="\e[0;32m"
        ["red"]="\e[0;31m"
        ["yellow"]="\e[0;33m"
        ["normal"]="\e[0m"
    )
    printf "${colors[$1]}%b${colors['normal']}\\n" "$2"
}

# Print string to stderr in red.
# Args: $1 (required): String to print.
function error() {
    args_number $# 1
    cprint "red" "$*" >&2
}

# Check if given binary is available.
# Args: $1 (required): Name of the binary.
function has() {
    args_number $# 1
    command -v "$1" >/dev/null 2>&1 || script_error "$1 binary not found"
}

# Check if script is run as root.
function is_root() {
    [[ $EUID -eq 0 ]] || script_error "Must be run as root!"
}

# Parse parameters.
# Args: $@ (optional): Arguments provided to the script.
function parse_params() {
    while [[ $# -gt 0 ]]; do
        case "$1" in
            -h | --help)
                usage
                exit 0
                ;;
            -p | --parameter)
                [ $# -eq 2 ] || script_error "The value for $1 is missing."
                readonly PARAMETER="$2"
                shift
                ;;
            *)
                script_error "Wrong parameters"
                ;;
        esac
        shift
    done
}

# Cleanly handle unexpected errors.
# Args: $1 (optional): Error message.
function script_error() {
    # Disable error handling, so we can exit cleanly.
    trap - ERR
    set +o errexit
    set +o pipefail
    error "${1:-}"
    usage
    exit 1
}

# Print string to stdout in green.
# Args: $1 (required): String to print.
function success() {
    args_number $# 1
    cprint "green" "$*" >&2
}

# Print string to stderr in yellow.
# Args: $1 (required): String to print.
function warning() {
    args_number $# 1
    cprint "yellow" "$*" >&2
}

# Print usage to stdout.
function usage() {
    sed -Ene 's/^### ?//;T;p' "$0"
}

# Run the script
main "$@"

# vim: syntax=sh cc=80 tw=79 ts=4 sw=4 sts=4 et sr
